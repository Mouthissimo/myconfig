# _______________________________________________________________
# _______________________________________________________________
# _____________________GLOBAL CONFIG_____________________________
# _______________________________________________________________








# i3 DEFAULTS


# xss-lock setup
exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork

# NetworkManager is the most popular way to manage wireless networks on Linux, and nm-applet is a desktop environment-independent system tray GUI for it.
exec --no-startup-id nm-applet

# Use pactl to adjust volume in PulseAudio.
set $refresh_i3status killall -SIGUSR1 i3status

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod








# NAMES


# Workspaces

set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"


# Mod key

set $mod Mod4


# Gaps

gaps inner 10px
gaps outer 1px


# System modes

set $mode_system SESSION : (e)xit, (l)ock, sw(i)tch user, (r)eboot, (s)hutdown, s(u)spend
set $mode_i3_tools I3 : re(l)oad i3, (r)estart i3, (h)elp, sh(o)rtcuts
set $mode_system_tools SYSTEM TOOLS : (a)udio, (b)luetooth, (i)nfo, (n)etwork, a(p)pearance, p(r)ocess
set $mode_app_launcher APPLICATIONS : (a)rchive manager, s(c)an, (f)ile explorer, (o)ffice, (s)creenshot, (t)erminal, (w)eb browser, te(x)t editor
set $mode_scripts_menu SCRIPTS : (s)ystem, (p)cloud, (b)rave, (o)ther


# Software

set $dft_terminal terminator
set $backup_terminal warp-terminal
set $dft_dmenu "rofi -show drun"
set $dft_exec "rofi -show run"
set $dft_system_process_manager btop
set $dft_text_editor nano

set $dft_GUI_file_explorer thunar
set $dft_GUI_Network_manager nm-connection-editor
set $dft_GUI_screenshot_interface xfce4-screenshooter
set $dft_GUI_audio_interface pavucontrol
set $dft_GUI_text_editor subl
set $dft_GUI_markdown_editor retext

set $web_browser_1 librewolf
set $web_browser_2 brave-browser-stable --password-store=basic




# Dual monitor support


# Display monitors

# use 'xrandr' to check visual displays available and their names
# Tutorial: https://fedoramagazine.org/using-i3-with-multiple-monitors/
# set $monitor1 "HDMI-0"
# set $monitor2 "DVI-I-0"
# set $monitor1 "Virtual1"
# set $monitor2 "Virtual2"


# Workspace outputs

# workspace $ws1 output $monitor1
# workspace $ws2 output $monitor1
# workspace $ws3 output $monitor1
# workspace $ws4 output $monitor1
# workspace $ws5 output $monitor1

# workspace $ws6 output $monitor2
# workspace $ws7 output $monitor2
# workspace $ws8 output $monitor2
# workspace $ws9 output $monitor2
# workspace $ws10 output $monitor2


# Display monitors setup

# exec xrandr --output $monitor2 --auto --right-of $monitor1








# PATHS


# Scripts

set $pth_scripts ~/myconfig/scripts


# Readme-help

set $pth_shortcuts ~/myconfig/shortcuts.md
set $pth_readme ~/myconfig/README.md
set $pth_sshConfig ~/.ssh/config


# Quick reminders

set $pth_quick_reminders ~/quick_reminders.md


# Wallpaper

set $pth_wallpaper ~/Pictures/wallpapers/*
set $pth_savescreen ~/Pictures/savescreens/*


# System Sounds

set $pth_sounds_session_startup ~/Music/System-sounds/session_startup.mp3
set $pth_sounds_session_logout ~/Music/System-sounds/session_logout.mp3
set $pth_sounds_session_shutdown ~/Music/System-sounds/session_shutdown.mp3
set $pth_sounds_switch_workspace ~/Music/System-sounds/switch_workspace.mp3
set $pth_sounds_move_to_workspace ~/Music/System-sounds/move_to_workspace.mp3
set $pth_sounds_mode_menu ~/Music/System-sounds/mode_menu.mp3
set $pth_sounds_launcher_menu ~/Music/System-sounds/launcher_menu.mp3
set $pth_sounds_app_startup ~/Music/System-sounds/app_startup.mp3
set $pth_sounds_app_close ~/Music/System-sounds/app_close.mp3
set $pth_sounds_audio_volume ~/Music/System-sounds/audio_volume.mp3




# RICE

# Bar & window fonts
font pango:IBM Plex Mono Regular 9
# font pango:Cascadia Code Regular 9

# Window layout
default_border pixel 1

# Colors
set $rc2_clr0 #2E3440
set $rc2_clr1 #ECEFF4
set $rc2_clr2 #434C5E
set $rc2_clr3 #D08770
set $rc2_clr4 #BF616A
set $rc2_clr5 #3B4252

# Window borders - border / background / text / indicator / child_border
client.focused $rc2_clr3 $rc2_clr3 $rc2_clr0 $rc2_clr3 $rc2_clr3
client.unfocused $rc2_clr5 $rc2_clr5 $rc2_clr1 $rc2_clr5 $rc2_clr5
client.focused_inactive $rc2_clr5 $rc2_clr5 $rc2_clr1 $rc2_clr5 $rc2_clr5
client.urgent $rc1_clr4 $rc1_clr4 $rc2_clr1 $rc1_clr4 $rc1_clr4








# BAR & MODES


# i3 bar

bar {

        # Bar layout
        position top
        workspace_buttons yes
        separator_symbol " "
        # (not working) tray_output $monitor1

        # Status
        status_command i3status

        # Bar colors
        colors {                
                
                # Bar background
                background $rc2_clr0
                
                # Bar text
                statusline $rc2_clr1

                # Workspace icons - border / background / text
                focused_workspace $rc2_clr3 $rc2_clr3 $rc2_clr0
                inactive_workspace $rc2_clr2 $rc2_clr2 $rc2_clr1
                urgent_workspace $rc2_clr4 $rc2_clr4 $rc2_clr1
                binding_mode $rc2_clr3 $rc2_clr3 $rc2_clr0

        }

}


# Exit mode

mode "$mode_system" {

        # Log out
        bindsym e  exec --no-startup-id i3-msg exit; exec mpg123 $pth_sounds_session_logout

        # Restart
        bindsym r  exec --no-startup-id systemctl reboot; exec mpg123 $pth_sounds_session_shutdown
    
        # Turn off
        bindsym s  exec --no-startup-id systemctl poweroff; exec mpg123 $pth_sounds_session_shutdown

        # Suspend
        bindsym u  exec --no-startup-id "dm-tool lock", exec --no-startup-id systemctl suspend, mode "default"
        # bindsym u  exec --no-startup-id i3lock -n -u -i $pth_savescreen -t, exec --no-startup-id systemctl suspend, mode "default"

        # Lock
        bindsym l  exec --no-startup-id "dm-tool lock", mode "default"
        # bindsym l  exec --no-startup-id i3lock -n -u -i $pth_savescreen -t, mode "default"

        # Switch User
        bindsym i  exec "dm-tool switch-to-greeter", mode "default"

        # Cancel
        bindsym Return mode "default"
        bindsym Escape mode "default"

}


# i3 tools mode

mode "$mode_i3_tools" {

        # Restart i3
        bindsym r restart, mode "default"

        # Reload i3
        bindsym l reload, mode "default"

        # Help
        bindsym h exec $dft_terminal -e "less $pth_readme", mode "default"; exec mpg123 $pth_sounds_app_startup

        # Shortcuts
        bindsym o exec $dft_terminal -e "less $pth_shortcuts", mode "default"; exec mpg123 $pth_sounds_app_startup

        # Cancel
        bindsym Return mode "default"
        bindsym Escape mode "default"

}


# System tools mode

mode "$mode_system_tools" {

        # Audio
        bindsym a exec $dft_GUI_audio_interface, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Bluetooth
        bindsym b exec blueman-manager, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Info
        bindsym i exec hardinfo, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Network
        bindsym n exec $dft_GUI_Network_manager, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Appearance
        bindsym p exec lxappearance, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Process Manager
        bindsym r exec $dft_terminal -e $dft_system_process_manager, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Cancel
        bindsym Return mode "default"
        bindsym Escape mode "default"


}


# App launcher mode

mode "$mode_app_launcher" {

        # Archive manager
        bindsym a exec file-roller, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Scan
        bindsym c exec simple-scan, mode "default"; exec mpg123 $pth_sounds_app_startup

        # File explorer
        bindsym f exec $dft_GUI_file_explorer, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Office
        bindsym o exec libreoffice, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Screenshot
        bindsym s exec $dft_GUI_screenshot_interface, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Terminal
        bindsym t exec $dft_terminal, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Web browser
        bindsym w exec $web_browser_1, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Text Editor
        bindsym x exec $dft_GUI_text_editor, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Cancel
        bindsym Return mode "default"
        bindsym Escape mode "default"

}


# Scripts menu mode

mode "$mode_scripts_menu" {

        # System
        bindsym s exec $dft_terminal -e $pth_scripts/system-script.sh, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Pcloud
        bindsym p exec $dft_terminal -e $pth_scripts/pcloud-script.sh, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Brave
        bindsym b exec $dft_terminal -e $pth_scripts/brave-script.sh, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Other
        bindsym o exec $dft_terminal -e $pth_scripts/other-script.sh, mode "default"; exec mpg123 $pth_sounds_app_startup

        # Cancel
        bindsym Return mode "default"
        bindsym Escape mode "default"

}








# WINDOWS & WORKSPACES


# Switch to workspace

# ---- SPLIT CONFIGURATION ----


# Move focused container to workspace

# ---- SPLIT CONFIGURATION ----


# Window navigation

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

bindsym $mod+Shift+Left move left; exec mpg123 $pth_sounds_move_to_workspace
bindsym $mod+Shift+Down move down; exec mpg123 $pth_sounds_move_to_workspace
bindsym $mod+Shift+Up move up; exec mpg123 $pth_sounds_move_to_workspace
bindsym $mod+Shift+Right move right; exec mpg123 $pth_sounds_move_to_workspace


# Window split

bindsym $mod+Tab split toggle
bindsym $mod+Shift+Tab layout toggle splitv splith


# Window resize

bindsym $mod+control+Right resize grow width 1 px or 1 ppt
bindsym $mod+control+Left resize shrink width 1 px or 1 ppt
bindsym $mod+control+Up resize shrink height 1 px or 1 ppt
bindsym $mod+control+Down resize grow height 1 px or 1 ppt


# Floating windows & Scratchpad

bindsym $mod+BackSpace scratchpad show; exec mpg123 $pth_sounds_switch_workspace
bindsym $mod+Shift+BackSpace move scratchpad; exec mpg123 $pth_sounds_move_to_workspace
bindsym $mod+control+BackSpace floating toggle








# STARTUP


# Lock screen

exec killall xss-lock
exec xss-lock -- i3lock -n -u -i $pth_savescreen -t
# exec xset s 600
# exec --no-startup-id xss-lock -- dm-tool lock


# Compositor

exec_always picom


# Wallpaper

# exec_always feh --bg-max $pth_wallpaper
# exec_always feh --bg-fill $pth_wallpaper
exec_always feh --randomize --bg-fill $pth_wallpaper


# Touchpad activation

# ---- SPLIT CONFIGURATION ----

# tutorial: https://major.io/2021/07/18/tray-icons-in-i3
# execute 'xinput' in terminal to show a list of all available input devices and find the name of your touchpad
# execute 'xinput list-props "Name of your Touchpad"' to find the name and id of its tapping property ('libinput Tapping Enabled')
# execute 'xinput set-prop "Name of your Touchpad" prop-id 1' to activate it


# Startup Sound

exec mpg123 $pth_sounds_session_startup








# SHORTCUTS: SYSTEM


# Menus

bindsym $mod+Escape mode "$mode_system"; exec mpg123 $pth_sounds_mode_menu
bindsym $mod+Shift+Escape mode "$mode_i3_tools"; exec mpg123 $pth_sounds_mode_menu
bindsym $mod+F1 mode "$mode_system_tools"; exec mpg123 $pth_sounds_mode_menu
bindsym $mod+F2 mode "$mode_app_launcher"; exec mpg123 $pth_sounds_mode_menu
bindsym $mod+F4 mode "$mode_scripts_menu"; exec mpg123 $pth_sounds_mode_menu


# Terminal

bindsym $mod+Return exec $dft_terminal; exec mpg123 $pth_sounds_app_startup
bindsym $mod+Shift+Return exec $backup_terminal; exec mpg123 $pth_sounds_app_startup


# Launcher menu

bindsym $mod+space exec $dft_dmenu; exec mpg123 $pth_sounds_launcher_menu
bindsym $mod+Shift+space exec $dft_exec; exec mpg123 $pth_sounds_launcher_menu


# Process manager

bindsym $mod+Delete --release exec --no-startup-id xkill; exec mpg123 $pth_sounds_app_startup
bindsym $mod+Shift+Delete exec $dft_terminal -e $dft_system_process_manager; exec mpg123 $pth_sounds_app_startup


# Readme-help

# ---- SPLIT CONFIGURATION ----


# Audio

# ---- SPLIT CONFIGURATION ----








# SHORTCUTS: A/Q -> P


# <a/q>
# ---- SPLIT CONFIGURATION ----

# <A/Q>
# ---- SPLIT CONFIGURATION ----


# <z/w>
# ---- SPLIT CONFIGURATION ----

# <Z/W>
# ---- SPLIT CONFIGURATION ----


# <e>
bindsym $mod+e exec weektodo; exec mpg123 $pth_sounds_app_startup

# <E>


# <r>
bindsym $mod+r exec $dft_GUI_text_editor; exec mpg123 $pth_sounds_app_startup

# <R>


# <t>
bindsym $mod+t exec flatpak run com.discordapp.Discord; exec mpg123 $pth_sounds_app_startup

# <T>


# <y>
bindsym $mod+y exec spotify; exec mpg123 $pth_sounds_app_startup

# <Y>


# <u>
bindsym $mod+u exec gitg; exec mpg123 $pth_sounds_app_startup

# <U>


# <i>
bindsym $mod+i exec deluge; exec mpg123 $pth_sounds_app_startup

# <I>


# <o>
bindsym $mod+o exec $dft_GUI_screenshot_interface; exec mpg123 $pth_sounds_app_startup

# <O>


# <p>
bindsym $mod+p exec $dft_GUI_screenshot_interface; exec mpg123 $pth_sounds_app_startup

# <P>








# SHORTCUTS: Q/A -> M/Ñ


# <q/a>
# ---- SPLIT CONFIGURATION ----

# <Q/A>
# ---- SPLIT CONFIGURATION ----


# <s>

# <S>


# <d>

# <D>


# <f>
bindsym $mod+f fullscreen toggle

# <F>


# <g>
# windows shortcut

# <G>
# windows shortcut


# <h>

# <H>


# <j>

# <J>


# <k>
bindsym $mod+k exec killall picom; exec mpg123 $pth_sounds_app_startup

# <K>
bindsym $mod+Shift+k exec picom; exec mpg123 $pth_sounds_app_startup


# <l>
# windows shortcut

# <L>
bindsym $mod+Shift+l exec --no-startup-id "dm-tool lock"
# bindsym $mod+Shift+l exec i3lock -n -u -i $pth_savescreen -t
# bindsym $mod+Shift+l exec i3lock -n -u -c 465264


# <m/ñ>
# ---- SPLIT CONFIGURATION ----

# <M/Ñ>
# ---- SPLIT CONFIGURATION ----








# W/Z -> N/M


# <w/z>
# ---- SPLIT CONFIGURATION ----

# <W/Z>
# ---- SPLIT CONFIGURATION ----


# <x>

# <X>


# <c>

# <C>


# <v>

# <V>


# <b>

# <B>


# <n>

# <N>
