# UNIXCONFIG (Debian)




## Packages & Software


### - GUI

- feh 			: desktop background manager
- i3  			: window tiling manager
- picom 		: compositor
- rofi 			: dmenu


### - Terminal Tools

- btop 			: system process monitor
- ffmpeg		: multimedia suite
- mpg123 		: audio player
- nano 			: terminal text editor
- net-tools 		: base networking tools
- openssh-server 	: SSH remote login tool
- psmisc		: utilities that use the proc file system (fuser, killall...)
- rsync			: incremental file transfer tool
- samba 		: SMB networking protocol
- screenfetch		: system information display
- xinput 		: configure and test X input devices (mice, keyboards, touchpads...)


### - Graphic Tools

- blueman 		: bluetooth interface
- eom 			: image viewer
- file-roller		: archive manager
- gitg 			: git client
- gparted 		: partition editor
- hardinfo 		: hardware information
- lxappearance 		: GTK theme switcher
- mpv 			: video player
- pavucontrol 		: audio interface
- retext 		: markdown text editor
- terminator 		: terminal emulator
- thunar 		: file manager
- vlc			: video player
- warp-terminal 	: AI terminal emulator




## Setup (general)


### - APT

If you're using Debian (vanilla), and want to install non-DFSG packages (necessary when dealing with Nvidia drivers for example), remember to enable "contrib" and "non-free" repositories. To do so, open '/etc/apt/sources.list' with a text editor, and append "non-free-firmware contrib non-free" to each source. For example:

```
deb http://deb.debian.org/debian/ buster main
deb-src http://deb.debian.org/debian/ buster main
```

Becomes:

```
deb http://deb.debian.org/debian/ buster main non-free-firmware contrib non-free
deb-src http://deb.debian.org/debian/ buster main non-free-firmware contrib non-free
```

Run 'apt update' after modifying sources of packages.

#### Mesa Library

Official Debian documentation: https://wiki.debian.org/Mesa

- Install with 'sudo apt update && sudo apt install mesa-utils' (and maybe reboot your system)

- Learn about the chip that your computer uses: 'glxinfo | grep OpenGL'. If in the 'OpenGL renderer string:' you see 'llvmpipe', that means your system doesn't use the GPU but the CPU instead to render the computer graphics.

- Determine whether 3D acceleration is working: 'glxinfo | grep rendering'. The output should be: 'direct rendering: Yes'.

#### Packages

- Deluge ('https://deluge-torrent.org/download/') : torrent client

- LibreWolf ('https://librewolf.net/installation/debian/') : web browser

- Brave Browser ('https://brave.com/linux/') : web browser (do not install any extensions yet)

- Sublime Text ('https://www.sublimetext.com/docs/linux_repositories.html') : GUI text editor

- Git ('sudo apt install git') : version control system

- Warp ('https://www.warp.dev/') : AI terminal emulator

- WeekToDo ('https://weektodo.me/download/') : weekly planner


### - Manage separate partition/drive

This link was helpful: 'https://community.linuxmint.com/tutorial/view/1609'. This is how I made it work:

- I first mounted the drive throught the default session (xfce at the time) using 'thunar'. This can also be done through the terminal (see link above).

- I then changed the ownership of all files and folders on the drive with 'sudo chown -R someone: /media/someone/Drive' ("someone" is my username, second parameter corresponds to the drive's mount point).

- I then looked for my drive's specific UUID using the command 'lsblk -af'.

- I finally added these two lines to '/etc/fstab' (using 'sudo subl'):

```
# Mount Drive partition under /media/someone/Drive
UUID=8daaa6a4-ddc2-4379-b556-fc0023a507c5 /media/someone/Drive     ext4     defaults     0     2
```

As long as the UUID, mount point and file system type are correct, this should work. To learn more about how 'fstab' works, follow this link: https://www.baeldung.com/linux/fstab-file-users-read-write-partition


### - Flatpak

Follow your distribution's quick setup guide: 'https://flatpak.org/setup/'

- 'sudo apt install flatpak'

- 'flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo'

- reboot your system

#### Packages

- Bitwarden ('flatpak install flathub com.bitwarden.desktop') : password manager

- Discord ('flatpak install flathub com.discordapp.Discord') : messaging, voice and video client

- Gear Lever ('flatpak install flathub it.mijorus.gearlever') : AppImage GUI manager

- Krita ('flatpak install flathub org.kde.krita') : digital art studio

- Proton VPN ('flatpak install flathub com.protonvpn.www') : VPN service client

- SysD Manager ('flatpak install flathub io.github.plrigaux.sysd-manager') : systemd units manager


### - AppImages

Use 'Gear Lever' to manage AppImages (right-click: Open With "Gear Lever").

#### Apps

- Obsidian ('https://obsidian.md/download') : note-taking app

- pCloud ('https://www.pcloud.com/download-free-online-cloud-file-storage.html') : cloud storage client

- Upscayl ('https://upscayl.org/download') : image AI upscaler




## NVIDIA drivers (Optional)


### - Installation

Official Debian documentation: https://wiki.debian.org/NvidiaGraphicsDrivers

- Identify the NVIDIA GPU series/codename with 'lspci -n -n -k | grep -A 2 -e VGA -e 3D'.

- Run 'sudo apt update && sudo apt install nvidia-detect'

- Run 'nvidia-detect'

- If things went smoothly, run 'sudo apt install nvidia-driver firmware-misc-nonfree', or check with the Debian documentation on what to do (link above).

- reboot your system


### - Set NVIDIA GPU as the primary GPU

The default behavior of most Linux distros is to use the iGPU (Intel) for pretty much everything and only use the dGPU (Nvidia) when the user specifically requests it. This is called offloading and is done to increase the battery life of a laptop.

- To render everything on the Nvidia card follow these instructions: https://wiki.debian.org/NVIDIA%20Optimus#Using_NVIDIA_GPU_as_the_primary_GPU

- Once done, you can check the current active GPU driver with 'glxinfo | grep -e OpenGL.vendor -e OpenGL.renderer'

- (Optional) run nvtop ('sudo apt install nvtop') to monitor GPU usage




## Setup (i3)


### - Files & Folders

- Paste 'myconfig' folder into '~/'.

- Paste 'home' folder into '~/myconfig/'. This is how the folder structure should look like:

```
home/
	bashrc/
	quick_reminders/
	ssh/
```

- Paste and extract all available resource folders into '~/myconfig/resources/'. This is how the folder structure should look like:

```
resources/
	display-manager/
	fonts/
	gtk_themes/
	icon_themes/
	mouse_cursors/
	savescreens/
	system-sounds/
	wallpapers/
```


### - Scripts

- Run 'cd ~/myconfig/scripts'.

- Run 'sudo chmod +x setup-script.sh'.

- Run './setup-script.sh'

- Run './refresh-script.sh'

- Run './display-manager-script.sh'


### - Appearance

- In 'lxappearance' select the theme, font and icons for the rice (these are specified in the 'Theme' section below). Use screenshots found in '~/myconfig/lxappearance-screenshots' for reference. Remember to log out of the current user session to apply changes.

- In 'chrome', go to 'settings' : 'appearance' and select 'use GTK'

- If needed, change 'LibreOffice' icon themes by opening any 'libreoffice' program and going into 'Tools : Options : {Name of the program} : View : Icon style' (+ there's a small contextual menu for downloading more icon themes)

- Place the Brave 'Bookmarks' and 'Preferences' files directly into '~/.config/BraveSoftware/Brave-Browser/Default/' (remember that 'Preferences' should be placed BEFORE installing browser extensions).


### - Configuration

- Any config changes should be made in '~/myconfig' directory.

- Changes made to the 'home' and 'resources' directories are part of the '.gitignore'. Therefore they should be backed up somewhere else outside of 'myconfig'.

- Execute 'scripts/refresh-script.sh' to replace 'config', 'home' and resource files and folders on your system.

- Log out if you want to visualize GTK theme changes.




## Theme


### - lxappearance

- GTK Theme				: Nordic-darker
- Font 					: Cascadia Code (Regular / 10)
- Icon Theme				: Nordzy-dark
- Mouse Cursor				: Nordic-cursors

- LibreOffice icon Style 		: Yaru

#### links

- https://www.gnome-look.org/p/1267246/

- https://github.com/microsoft/cascadia-code/releases/tag/v2404.23

- https://www.gnome-look.org/s/Gnome/p/1686927

- https://www.gnome-look.org/p/1662218/

- https://extensions.libreoffice.org/en/extensions/show/yaru-icon-theme


### - colors

https://www.nordtheme.com/docs/colors-and-palettes

- rc1_clr0 : #2E3440	rgb(46, 52, 64)			very dark blue (almost black)
- rc1_clr1 : #ECEFF4	rgb(236, 239, 244)		white (very slight cyan-ish-grey)
- rc1_clr2 : #434C5E	rgb(67, 76, 94)			medium dark blue
- rc1_clr3 : #D08770	rgb(208, 135, 112)		orange
- rc1_clr4 : #BF616A	rgb(191, 97, 106)		red
- rc1_clr5 : #3B4252	rgb(59, 66, 82)			pretty dark blue
- rcl_clr6 : #81A1C1	rgb(129, 161, 193)		pale blue

#### extra terminator colors

- #B48EAD
- #E5E9F0
- #EBCB8B
- #8FBCBB


### - "nordify" background

https://ign.schrodinger-hat.it/getting-started


### - extra fonts

- IBM Plex Mono ('https://fonts.google.com/specimen/IBM+Plex+Mono')
