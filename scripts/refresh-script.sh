#!/bin/bash




# remove directories & files

sudo -E rm -r -v ~/.config/btop
sudo -E rm -r -v ~/.config/i3
sudo -E rm -r -v ~/.config/i3status
sudo -E rm -r -v ~/.config/picom
sudo -E rm -r -v ~/.config/ReText\ project
sudo -E rm -r -v ~/.config/rofi
sudo -E rm -r -v ~/.config/terminator
sudo -E rm -r -v ~/.fonts
sudo -E rm -r -v ~/.i3
sudo -E rm -r -v ~/Music/System-sounds
sudo -E rm -r -v ~/Pictures/savescreens
sudo -E rm -r -v ~/Pictures/wallpapers
sudo -E rm -r ~/.icons
sudo -E rm -r ~/.themes
sudo -E rm -v ~/.bashrc
sudo -E rm -v ~/.ssh/config
sudo -E rm -v ~/quick_reminders.md


# add directories

sudo -E mkdir -v ~/.config/btop
sudo -E mkdir -v ~/.config/btop/themes
sudo -E mkdir -v ~/.config/i3status
sudo -E mkdir -v ~/.config/picom
sudo -E mkdir -v ~/.config/ReText\ project
sudo -E mkdir -v ~/.config/rofi
sudo -E mkdir -v ~/.config/terminator
sudo -E mkdir -v ~/.fonts
sudo -E mkdir -v ~/.i3
sudo -E mkdir -v ~/.icons
sudo -E mkdir -v ~/.ssh
sudo -E mkdir -v ~/.themes
sudo -E mkdir -v ~/Music
sudo -E mkdir -v ~/Music/System-sounds
sudo -E mkdir -v ~/Pictures
sudo -E mkdir -v ~/Pictures/savescreens
sudo -E mkdir -v ~/Pictures/wallpapers




# go to main folder

cd ~/myconfig


# add config files

sudo -E cp -v dotfiles/btop/btop.conf ~/.config/btop
sudo -E cp -v dotfiles/btop/myconfig.theme ~/.config/btop/themes
sudo -E cp -v dotfiles/i3/config ~/.i3
sudo -E cp -v dotfiles/i3/i3status/config ~/.config/i3status
sudo -E cp -v dotfiles/picom/picom.conf ~/.config/picom
sudo -E cp -v dotfiles/ReText/ReText.conf ~/.config/ReText\ project
sudo -E cp -v dotfiles/rofi/config.rasi ~/.config/rofi
sudo -E cp -v dotfiles/terminator/config ~/.config/terminator


# add home files

sudo -E cp -v home/bashrc/.bashrc ~/
sudo -E cp -v home/ssh/config ~/.ssh
sudo -E cp -v home/quick_reminders/quick_reminders.md ~/


# add resources

sudo -E cp -R -v resources/fonts/* ~/.fonts
sudo -E cp -R -v resources/savescreens/* ~/Pictures/savescreens
sudo -E cp -R -v resources/wallpapers/* ~/Pictures/wallpapers
sudo -E cp -R -v resources/system-sounds/* ~/Music/System-sounds
sudo -E cp -R resources/icon_themes/* ~/.icons
sudo -E cp -R resources/mouse_cursors/* ~/.icons
sudo -E cp -R resources/gtk_themes/* ~/.themes




# change folder/file permissions

sudo -E chown -R -v $(whoami):$(whoami) ~/.bashrc
sudo -E chown -R -v $(whoami):$(whoami) ~/.config/btop
sudo -E chown -R -v $(whoami):$(whoami) ~/.config/i3status
sudo -E chown -R -v $(whoami):$(whoami) ~/.config/picom
sudo -E chown -R -v $(whoami):$(whoami) ~/.config/ReText\ project
sudo -E chown -R -v $(whoami):$(whoami) ~/.config/rofi
sudo -E chown -R -v $(whoami):$(whoami) ~/.config/terminator
sudo -E chown -R -v $(whoami):$(whoami) ~/.fonts
sudo -E chown -R -v $(whoami):$(whoami) ~/.i3
sudo -E chown -R -v $(whoami):$(whoami) ~/.ssh
sudo -E chown -R -v $(whoami):$(whoami) ~/Music
sudo -E chown -R -v $(whoami):$(whoami) ~/Music/System-sounds
sudo -E chown -R -v $(whoami):$(whoami) ~/Pictures
sudo -E chown -R -v $(whoami):$(whoami) ~/Pictures/savescreens
sudo -E chown -R -v $(whoami):$(whoami) ~/Pictures/wallpapers
sudo -E chown -R -v $(whoami):$(whoami) ~/quick_reminders.md
sudo -E chown -R $(whoami):$(whoami) ~/.icons
sudo -E chown -R $(whoami):$(whoami) ~/.themes




# restart window manager

# i3 restart




# prevents terminal from closing

$SHELL
