#!/bin/bash


echo "-- System Script --"
echo " "

PS3='Choose your option: '
options=("Hardware information" "System information" "System Update" "System Cleanup" "Apt Packages" "Flatpak Packages" "Exit")
optionsDisplay="1) Hardware information  2) System information  3) System Update  4) System Cleanup  5) Apt Packages  6) Flatpak Packages  7) Exit"

select selectedOption in "${options[@]}"; do
    case $selectedOption in
        "Hardware information")
            sudo lspci -vnnn
            sudo cat /sys/devices/virtual/dmi/id/*
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "System information")
            hostnamectl
            screenfetch
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "System Update")
            sudo apt update
            sudo apt dist-upgrade -y
            sudo flatpak update -y
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "System Cleanup")
            sudo apt autoclean
            sudo apt autoremove -y
            flatpak uninstall --unused
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Apt Packages")
            dpkg --get-selections | less
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Flatpak Packages")
            flatpak list
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
	"Exit")
	    echo "Exit"
        $SHELL
	    exit
	    ;;
        *) echo "Invalid option $REPLY";;
    esac
done
