#!/bin/bash


echo " "
echo "-- System Script --"

PS3='Choose your option: '
options=("Hardware information" "System information" "System Update" "System Cleanup" "Exit")

select selectedOption in "${options[@]}"; do
    case $selectedOption in
        "Hardware information")
            sudo lspci -vnnn
            sudo cat /sys/devices/virtual/dmi/id/*
            $SHELL
            exit
            ;;
        "System information")
            hostnamectl
            screenfetch
            $SHELL
            exit
            ;;
        "System Update")
            sudo apt update
            sudo apt dist-upgrade -y
            sudo flatpak update -y
            $SHELL
            exit
            ;;
        "System Cleanup")
            sudo apt autoclean
            sudo apt autoremove -y
            flatpak uninstall --unused
	        $SHELL
            exit
	    break
            ;;
	"Exit")
	    echo "Exit"
        $SHELL
	    exit
	    ;;
        *) echo "Invalid option $REPLY";;
    esac
done
