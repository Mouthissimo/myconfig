#!/bin/bash


sudo -E apt update


# install software (GUI)

sudo -E apt install -y feh
sudo -E apt install -y i3
sudo -E apt install -y picom
sudo -E apt install -y rofi


# install software (Terminal Tools)

sudo -E apt install -y btop
sudo -E apt install -y ffmpeg
# sudo -E apt install -y htop
sudo -E apt install -y mpg123
sudo -E apt install -y nano
sudo -E apt install -y net-tools
sudo -E apt install -y openssh-server
sudo -E apt install -y psmisc
sudo -E apt install -y rsync
sudo -E apt install -y samba
sudo -E apt install -y screenfetch
sudo -E apt install -y xinput


# install software (Graphic Tools)

sudo -E apt install -y blueman
# sudo -E apt install -y cool-retro-term
sudo -E apt install -y eom
sudo -E apt install -y file-roller
sudo -E apt install -y gitg
sudo -E apt install -y gparted
sudo -E apt install -y hardinfo
sudo -E apt install -y lxappearance
sudo -E apt install -y mpv
sudo -E apt install -y pavucontrol
sudo -E apt install -y retext
sudo -E apt install -y simple-scan
sudo -E apt install -y terminator
sudo -E apt install -y thunar
sudo -E apt install -y vlc


# remove gnome keyring

sudo -E rm -r -v ~/.local/share/keyrings/*
sudo -E apt purge gnome-keyring


# scripts permissions

sudo -E chmod +x -v ~/myconfig/scripts/*


# prevents terminal from closing

$SHELL
