#!/bin/bash




# remove directories & files

sudo rm -r -v /etc/lightdm/lightdm-gtk-greeter.conf
sudo rm -r -v /usr/share/backgrounds/pixmax
sudo rm -r /usr/share/fonts/otf
sudo rm -r /usr/share/fonts/ttf
sudo rm -r /usr/share/fonts/woff2
sudo rm -r /usr/share/icons/Nordic-cursors
sudo rm -r /usr/share/icons/Nordzy-dark
sudo rm -r /usr/share/themes/Nordic-darker


# add directories

sudo mkdir -v /usr/share/backgrounds/pixmax




# go to main folder

cd ~/myconfig


# add lightdm config

sudo cp -v dotfiles/lightdm/lightdm-gtk-greeter.conf /etc/lightdm


# add resources

sudo cp -r -v resources/display-manager/* /usr/share/backgrounds/pixmax/
sudo cp -r resources/fonts/otf /usr/share/fonts/
sudo cp -r resources/fonts/ttf /usr/share/fonts/
sudo cp -r resources/fonts/woff2 /usr/share/fonts/
sudo cp -r resources/gtk_themes/Nordic-darker /usr/share/themes/
sudo cp -r resources/icon_themes/Nordzy-dark /usr/share/icons/
sudo cp -r resources/mouse_cursors/Nordic-cursors /usr/share/icons/




# change folder/file permissions

sudo chmod -R -v 644 /usr/share/backgrounds/pixmax/*
sudo chmod -R +xr /usr/share/fonts/otf
sudo chmod -R +xr /usr/share/fonts/ttf
sudo chmod -R +xr /usr/share/fonts/woff2
sudo chmod -R +xr /usr/share/icons/Nordic-cursors
sudo chmod -R +xr /usr/share/icons/Nordzy-dark
sudo chmod -R +xr /usr/share/themes/Nordic-darker




# restart session/lightdm

sudo systemctl restart lightdm

# $SHELL
