#!/bin/bash


echo "-- Pcloud Script --"
echo " "

PS3='Choose your option: '
options=("Dry backup" "Dry restore" "Dry family restore" "Full backup" "Full restore" "Full family restore" "Exit")
optionsDisplay="1) Dry backup  2) Dry restore 3)  Dry family restore  4) Full backup  5) Full restore  6) Full family restore  7) Exit"

select selectedOption in "${options[@]}"; do
    case $selectedOption in
        "Dry backup")
            rsync --dry-run -avz /media/$USER/Drive/[0\ -\ rsync-adrien]/ /home/$USER/pCloudDrive/[Adrien\ Cloud]/ --progress --delete --log-file=$HOME/.rsyncd.log
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Dry restore")
            rsync --dry-run -avz /home/$USER/pCloudDrive/[Adrien\ Cloud]/ /media/$USER/Drive/[0\ -\ rsync-adrien]/ --delete
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Dry family restore")
            rsync --dry-run -avz /home/$USER/pCloudDrive/[Family\ Cloud]/ /media/$USER/Drive/[0\ -\ rsync-family]/ --delete
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Full backup")
            read -e -p "!!! WARNING !!! Overwriting server data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rsync -avz /media/$USER/Drive/[0\ -\ rsync-adrien]/ /home/$USER/pCloudDrive/[Adrien\ Cloud]/ --progress --delete --log-file=$HOME/.rsyncd.log || exit 0
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Full restore")
            read -e -p "!!! WARNING !!! Overwriting local data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rsync -avz /home/$USER/pCloudDrive/[Adrien\ Cloud]/ /media/$USER/Drive/[0\ -\ rsync-adrien]/ --delete || exit 0
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Full family restore")
            read -e -p "!!! WARNING !!! Overwriting local data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rsync -avz /home/$USER/pCloudDrive/[Family\ Cloud]/ /media/$USER/Drive/[0\ -\ rsync-family]/ --delete || exit 0
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
	"Exit")
	    echo "Exit"
	    $SHELL
        exit
	    ;;
        *) echo "Invalid option $REPLY";;
    esac
done
