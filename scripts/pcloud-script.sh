#!/bin/bash


echo " "
echo "-- Pcloud Script --"

PS3='Choose your option: '
options=("Dry backup" "Dry restore" "Dry family restore" "Full backup" "Full restore" "Full family restore" "Exit")

select selectedOption in "${options[@]}"; do
    case $selectedOption in
        "Dry backup")
            rsync --dry-run -avz /media/$USER/Drive/[0\ -\ rsync-adrien]/ /home/$USER/pCloudDrive/[Adrien\ Cloud]/ --progress --delete --log-file=$HOME/.rsyncd.log
            $SHELL
            exit
            ;;
        "Dry restore")
            rsync --dry-run -avz /home/$USER/pCloudDrive/[Adrien\ Cloud]/ /media/$USER/Drive/[0\ -\ rsync-adrien]/ --delete
            $SHELL
            exit
            ;;
        "Dry family restore")
            rsync --dry-run -avz /home/$USER/pCloudDrive/[Family\ Cloud]/ /media/$USER/Drive/[0\ -\ rsync-family]/ --delete
            $SHELL
            exit
            ;;
        "Full backup")
            read -e -p "!!! WARNING !!! Overwriting server data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rsync -avz /media/$USER/Drive/[0\ -\ rsync-adrien]/ /home/$USER/pCloudDrive/[Adrien\ Cloud]/ --progress --delete --log-file=$HOME/.rsyncd.log || exit 0
            $SHELL
            exit
            ;;
        "Full restore")
            read -e -p "!!! WARNING !!! Overwriting local data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rsync -avz /home/$USER/pCloudDrive/[Adrien\ Cloud]/ /media/$USER/Drive/[0\ -\ rsync-adrien]/ --delete || exit 0
            $SHELL
            exit
            ;;
        "Full family restore")
            read -e -p "!!! WARNING !!! Overwriting local data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rsync -avz /home/$USER/pCloudDrive/[Family\ Cloud]/ /media/$USER/Drive/[0\ -\ rsync-family]/ --delete || exit 0
	        $SHELL
            exit
	    break
            ;;
	"Exit")
	    echo "Exit"
	    $SHELL
        exit
	    ;;
        *) echo "Invalid option $REPLY";;
    esac
done
