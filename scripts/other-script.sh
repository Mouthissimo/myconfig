#!/bin/bash


echo "-- Other Script --"
echo " "

PS3='Choose your option: '
options=("Delete recent files cache" "Delete Spotify cache" "Exit")
optionsDisplay="1) Delete recent files cache  2) Delete Spotify cache 3) Exit"

select selectedOption in "${options[@]}"; do
    case $selectedOption in
        "Delete recent files cache")
            rm -r -v ~/.local/share/recently-used.xbel
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Delete Spotify cache")
            rm -r -v /home/$USER/.cache/spotify/Storage/*
            rm -r /home/$USER/.cache/spotify/Users/*
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
	"Exit")
	    echo "Exit"
        $SHELL
	    exit
	    ;;
        *) echo "Invalid option $REPLY";;
    esac
done
