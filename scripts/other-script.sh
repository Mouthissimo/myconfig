#!/bin/bash


echo " "
echo "-- Other Script --"

PS3='Choose your option: '
options=("Delete recent files cache" "Delete Spotify cache" "Exit")

select selectedOption in "${options[@]}"; do
    case $selectedOption in
        "Delete recent files cache")
            rm -r -v ~/.local/share/recently-used.xbel
            $SHELL
            exit
            ;;
        "Delete Spotify cache")
            rm -r -v /home/$USER/.cache/spotify/Storage/*
            rm -r /home/$USER/.cache/spotify/Users/*
            $SHELL
            exit
	    break
            ;;
	"Exit")
	    echo "Exit"
        $SHELL
	    exit
	    ;;
        *) echo "Invalid option $REPLY";;
    esac
done
