#!/bin/bash


echo "-- Brave Script --"
echo " "

PS3='Choose your option: '
options=("Brave session backup" "Brave session restore" "Brave bookmarks backup" "Brave bookmarks restore" "Exit")
optionsDisplay="1) Brave session backup  2) Brave session restore  3) Brave bookmarks backup  4) Brave bookmarks restore  5) Exit"

select selectedOption in "${options[@]}"; do
    case $selectedOption in
        "Brave session backup")
            read -e -p "!!! WARNING !!! Overwriting server data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rm -r -v "/home/$USER/pCloudDrive/[Adrien Temp]/[0 - IT]/myconfig/brave-browser/Sessions" && cp -r -v "/home/$USER/.config/BraveSoftware/Brave-Browser/Default/Sessions" "/home/$USER/pCloudDrive/[Adrien Temp]/[0 - IT]/myconfig/brave-browser/" || exit 0
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Brave session restore")
            read -e -p "!!! WARNING !!! Overwriting local data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rm -r -v "/home/$USER/.config/BraveSoftware/Brave-Browser/Default/Sessions" && cp -r -v "/home/$USER/pCloudDrive/[Adrien Temp]/[0 - IT]/myconfig/brave-browser/Sessions" "/home/$USER/.config/BraveSoftware/Brave-Browser/Default/" || exit 0
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Brave bookmarks backup")
            read -e -p "!!! WARNING !!! Overwriting server data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rm -v "/home/$USER/pCloudDrive/[Adrien Temp]/[0 - IT]/myconfig/brave-browser/Bookmarks" && cp -v "/home/$USER/.config/BraveSoftware/Brave-Browser/Default/Bookmarks" "/home/$USER/pCloudDrive/[Adrien Temp]/[0 - IT]/myconfig/brave-browser/" || exit 0
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
        "Brave bookmarks restore")
            read -e -p "!!! WARNING !!! Overwriting local data. Continue? (y)" choice
            [[ "$choice" == [Yy]* ]] && rm -r -v "/home/$USER/.config/BraveSoftware/Brave-Browser/Default/Bookmarks" && cp -r -v "/home/$USER/pCloudDrive/[Adrien Temp]/[0 - IT]/myconfig/brave-browser/Bookmarks" "/home/$USER/.config/BraveSoftware/Brave-Browser/Default/" || exit 0
            echo " "
            echo " "
            echo "$optionsDisplay"
            ;;
	"Exit")
	    echo "Exit"
        $SHELL
	    exit
	    ;;
        *) echo "Invalid option $REPLY";;
    esac
done
