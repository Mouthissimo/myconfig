# KEYBOARD SHORTCUTS (Debian)




- {MOD} + Space			: open dmenu (rofi)
				* open run menu (rofi)

- {MOD} + Enter			: open terminal

- {MOD} + a			: open file explorer

- {MOD} + q			: close current window

- {MOD} + Delete		: kill window with mouse (xkill)
				* open system process monitor (htop)

- {MOD} + LeftShift + l		: lock screen

- {MOD} + s			: restart i3

- {MOD} + d			: execute '~/myconfig/refresh-script.sh'
				* execute '~/myconfig/setup-script.sh'

- {MOD} + v 			: execute update script
				* execute cleanup script

- {MOD} + o/p			: take screenshot (xfce4)




## Help


- {MOD} + ,			: display keyboard shortcuts
				* display readme file

- {MOD} + ;			: display SSH host aliases

- {MOD} + ! 			: display quick reminders




## Menus


- {MOD} + Escape		: (e)xit/(r)eboot/(s)hutdown/s(u)spend/(l)ock menu
				* i3/scripts menu

- {MOD} + F1 			: system tools menu

- {MOD} + F2 			: applications menu




## UI Navigation


- {MOD} + '2'			: go to workspace 2
				* move current window to workspace 2

- {MOD} + [→↑←↓]		: focus adjacent window
				* switch current window with adjacent window

- {MOD} + Tab			: next window opens vertically/horizontally (toggle)
				* switch current window to vertical/horizontal layout (toggle)

- {MOD} + LeftCtrl + [→↑←↓]	: resize current window

- {MOD} + f			: fullscreen

- {MOD} + k			: disable compositor
				* enable compositor

- {MOD} + m			: show/hide i3 status bar

- {MOD} + backspace 		: show/hide scratchpad
				* send current window to scratchpad
				(+ ctrl) remove window from scratchpad




## Audio


- {MOD} + ^			: increase volume

- {MOD} + ù			: decrease volume

- {MOD} + $			: mute audio

- {MOD} + *			: open audio GUI interface (pavucontrol)




## Applications


- {MOD} + z			: open web browser 1 (firefox)
				* open web browser 2 (brave-browser)

- {MOD} + e			: open GUI markdown editor (retext)

- {MOD} + r			: open GUI text editor (subl)

- {MOD} + y			: open spotify

- {MOD} + u			: open GUI git client (gitg)

- {MOD} + b			: open archive manager

- {MOD} + n 			: open GTK theme editor (lxappearance)




## Nano


- LeftCtrl + s			: save current file

- LeftCtrl + o			: write file ("Save as")

- LeftCtrl + q			: exit

- Pg Up				: scroll up

- Pg Down			: scroll down

- LeftCtrl + c			: copy current line

- LeftCtrl + x			: cut current line

- LeftCtrl + v			: paste contents

- LeftCtrl + a			: text selection mode

- LeftCtrl + z			: undo

- LeftCtrl + y			: redo

- LeftCtrl + f			: search mode ('LeftCtrl + n'  next / 'LeftCtrl + b' previous)

- LeftCtrl + r			: replace mode




## Terminal Commands


- xev					: find Key names for shortcut mapping
						(tutorial: http://xahlee.info/linux/linux_show_keycode_keysym.html)

- xprop					: find application class name for a specific window
						(under 'WM_CLASS (STRING)')

- xinput				: list all input devices available on the system

- xrandr				: list all visual displays available on the system

- startx i3				: start i3 tiling wm from terminal

- screenfetch				: fetch and display system information

- sudo useradd -m 'username'		: creates new user 'username'

- sudo passwd 'username'		: sets password for user 'username'

- sudo userdel -r 'username'		: delete user 'username'
